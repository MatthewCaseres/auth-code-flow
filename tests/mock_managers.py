from auth_code_flow import AbstractBaseFlowManager


class FlowManagerWithAbstractMethods(AbstractBaseFlowManager):
    base_uri = "https://authorization-server.com/oauth"
    client_id = "a-client-id"
    client_secret = "a-client-secret"
    redirect_uri = "https://client-server.com/callback"
    scope = "read_api write_api"


class FlowManager(AbstractBaseFlowManager):
    base_uri = "https://authorization-server.com/oauth"
    client_id = "a-client-id"
    client_secret = "a-client-secret"
    redirect_uri = "https://client-server.com/callback"
    scope = "read_api write_api"

    def store_user_state(self, user, state):
        ...

    def check_user_state(self, user, state):
        ...
