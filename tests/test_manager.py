from urllib.parse import parse_qs, urlparse

import pytest
import requests

from auth_code_flow.exceptions import (
    AuthCodeFlowError,
    ImproperlyConfigured,
    StateForgeryError,
)


def test_abstract_methods(
    manager_class_with_abstract_methods,
):
    with pytest.raises(TypeError) as excinfo:
        manager_class_with_abstract_methods()

    exception_str = str(excinfo.value)
    assert "store_user_state" in exception_str
    assert "check_user_state" in exception_str


def test_required_attributes(manager_class):
    required_attributes = [
        "authorization_path",
        "base_uri",
        "client_id",
        "client_secret",
        "grant_type",
        "redirect_uri",
        "response_type",
        "scope",
    ]
    for attribute in required_attributes:

        class Manager(manager_class):
            pass

        setattr(Manager, attribute, None)

        with pytest.raises(ImproperlyConfigured) as excinfo:
            Manager()

        assert attribute in str(excinfo.value)


def test_get_authorization_endpoint(manager, state, mocker):
    mock_get_params = mocker.patch.object(
        manager, "get_authorization_endpoint_params", autospec=True
    )
    mock_urlencode = mocker.patch(
        "auth_code_flow.managers.urlencode", return_value="key=value"
    )

    url = manager.get_authorization_endpoint(state=state)
    parts = urlparse(url)

    assert (
        parts.scheme + "://" + parts.netloc + parts.path
        == manager.base_uri + manager.authorization_path
    )
    mock_urlencode.assert_called_once_with(mock_get_params.return_value)


def test_get_authorization_endpoint_params(manager, state):
    query_params = manager.get_authorization_endpoint_params(state=state)

    assert query_params["client_id"] == manager.client_id
    assert query_params["redirect_uri"] == manager.redirect_uri
    assert query_params["response_type"] == manager.response_type
    assert query_params["scope"] == manager.scope
    assert query_params["state"] == state


def test_get_access_token_endpoint(manager):
    url = manager.get_access_token_endpoint()

    assert url == manager.base_uri + manager.access_token_path


def test_get_access_token_endpoint_params(manager, code, state):
    params = manager.get_access_token_endpoint_params(code=code, state=state)

    assert params["client_id"] == manager.client_id
    assert params["client_secret"] == manager.client_secret
    assert params["code"] == code
    assert params["grant_type"] == manager.grant_type
    assert params["redirect_uri"] == manager.redirect_uri
    assert params["state"] == state


@pytest.mark.usefixtures("access_token_request_server")
def test_fetch_access_token(
    manager, user, code, state, access_token_response_json, mocker
):
    mock_check_state = mocker.patch.object(
        manager, "check_user_state", return_value=True, autospec=True
    )

    resp = manager.fetch_access_token(user=user, code=code, state=state)

    assert resp.status_code == 200
    mock_check_state.assert_called_once_with(user=user, state=state)

    resp_json = resp.json()
    assert resp_json == access_token_response_json

    posted_params = resp.request.json()
    assert posted_params["client_id"] == manager.client_id
    assert posted_params["client_secret"] == manager.client_secret
    assert posted_params["code"] == code
    assert posted_params["grant_type"] == manager.grant_type
    assert posted_params["redirect_uri"] == manager.redirect_uri
    assert posted_params["state"] == state


@pytest.mark.usefixtures("access_token_request_server")
def test_fetch_access_token_for_payload_posted_as_form_data(
    manager, user, code, state, access_token_response_json, mocker
):
    mock_check_state = mocker.patch.object(
        manager, "check_user_state", return_value=True, autospec=True
    )

    resp = manager.fetch_access_token(
        user=user, code=code, state=state, post_form_data=True
    )

    assert resp.status_code == 200
    mock_check_state.assert_called_once_with(user=user, state=state)

    resp_json = resp.json()
    assert resp_json == access_token_response_json

    posted_urlencoded_data = resp.request.body
    posted_params = parse_qs(posted_urlencoded_data)
    assert posted_params["client_id"] == [manager.client_id]
    assert posted_params["client_secret"] == [manager.client_secret]
    assert posted_params["code"] == [code]
    assert posted_params["grant_type"] == [manager.grant_type]
    assert posted_params["redirect_uri"] == [manager.redirect_uri]
    assert posted_params["state"] == [state]


@pytest.mark.usefixtures("access_token_request_server")
def test_fetch_access_token_without_checking_state(
    manager, user, code, state, access_token_response_json, mocker
):
    mock_check_state = mocker.patch.object(manager, "check_user_state", autospec=True)

    resp = manager.fetch_access_token(
        user=user, code=code, state=state, check_state=False
    )

    assert resp.status_code == 200
    mock_check_state.assert_not_called()

    resp_json = resp.json()
    assert resp_json == access_token_response_json

    posted_params = resp.request.json()
    assert posted_params["client_id"] == manager.client_id
    assert posted_params["client_secret"] == manager.client_secret
    assert posted_params["code"] == code
    assert posted_params["grant_type"] == manager.grant_type
    assert posted_params["redirect_uri"] == manager.redirect_uri
    assert posted_params["state"] == state


def test_fetch_access_token_fails_for_invalid_state(manager, user, code, state, mocker):
    mocker.patch.object(manager, "check_user_state", return_value=False, autospec=True)

    with pytest.raises(StateForgeryError):
        manager.fetch_access_token(user=user, code=code, state=state)


@pytest.mark.usefixtures("access_token_request_http_error_server")
def test_fetch_access_token_fails_for_response_with_non_OK_status(
    manager, user, code, state, mocker
):
    mocker.patch.object(manager, "check_user_state", return_value=True, autospec=True)

    with pytest.raises(AuthCodeFlowError) as excinfo:
        manager.fetch_access_token(user=user, code=code, state=state)

    exception = excinfo.value
    assert exception.response is not None
    assert isinstance(exception.response, requests.Response)


@pytest.mark.usefixtures("access_token_request_error_server")
def test_fetch_access_token_fails_for_request_exception(
    manager, user, code, state, mocker
):
    mocker.patch.object(manager, "check_user_state", return_value=True, autospec=True)

    with pytest.raises(AuthCodeFlowError) as excinfo:
        manager.fetch_access_token(user=user, code=code, state=state)

    exception = excinfo.value
    assert exception.response is None
