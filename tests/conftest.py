from unittest import mock

import pytest
import requests

from .mock_managers import FlowManager, FlowManagerWithAbstractMethods


@pytest.fixture(scope="module")
def manager_class_with_abstract_methods():
    return FlowManagerWithAbstractMethods


@pytest.fixture(scope="module")
def manager_class():
    return FlowManager


@pytest.fixture(scope="module")
def manager(manager_class):
    return manager_class()


@pytest.fixture
def user():
    return "mock@user.com"


@pytest.fixture
def state():
    return "a mock state"


@pytest.fixture
def code():
    return "an authorization grant code"


@pytest.fixture(scope="module")
def access_token_response_json():
    return {
        "access_token": "an-access-token",
        "created_at": 1618201369,
        "refresh_token": "a-refresh-token",
        "scope": "read_api write_api",
        "token_type": "Bearer",
    }


@pytest.fixture
def access_token_request_server(manager, access_token_response_json, requests_mock):
    with requests.Session() as mock_session:
        with mock.patch("requests.Session", autospec=True, return_value=mock_session):
            requests_mock.register_uri(
                "POST",
                manager.base_uri + manager.access_token_path,
                json=access_token_response_json,
            )
            mock_session.mount("https://", requests_mock._adapter)
            mock_session.mount = lambda prefix, adapter: None

            yield


@pytest.fixture(params=[400, 401, 403, 404])
def access_token_request_http_error_server(request, manager, requests_mock):
    with requests.Session() as mock_session:
        with mock.patch("requests.Session", autospec=True, return_value=mock_session):
            requests_mock.register_uri(
                "POST",
                manager.base_uri + manager.access_token_path,
                status_code=request.param,
            )
            mock_session.mount("https://", requests_mock._adapter)
            mock_session.mount = lambda prefix, adapter: None

            yield


@pytest.fixture
def access_token_request_error_server(manager, requests_mock):
    with requests.Session() as mock_session:
        with mock.patch("requests.Session", autospec=True, return_value=mock_session):
            requests_mock.register_uri(
                "POST",
                manager.base_uri + manager.access_token_path,
                exc=requests.RequestException,
            )
            mock_session.mount("https://", requests_mock._adapter)
            mock_session.mount = lambda prefix, adapter: None

            yield
